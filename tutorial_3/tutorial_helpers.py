import glob
import os

import matplotlib.pyplot as plt
import pywasp as pw
import windkit.binned_wind_climate as bwc
import xarray as xr
from windkit.spatial import reproject, to_point

# from ipywidgets import interact, interactive, fixed, interact_manual
# import ipywidgets as widgets
# display(interact(change_map, x=['CORINE z0','CORINE landcover','Sentinel land cover']))
# def change_map(x):
#     return allmaps[x]

def read_tabfiles_oesterild():
    """Reads tabfiles from Østerild

    This function reads tabfiles from Østerild by
    using glob to list all the files having a particular
    pattern, looping of the matching files and appending
    them to a xarray dataset. Finally we convert the
    lat/lon to the projection using in our maps with the
    EPSG string 32632, i.e. UTM projection of zone 32N.

    Returns
    -------
    bothmasts_utm: xr.DataSet
        DataSet with all binned wind climates from both masts

    """
    obs = []
    for keymast in ['1','2']:
        dict_tabs = []
        files = glob.glob(os.path.join("data/tabfiles",'oesterild'+keymast+'_*.tab'))
        files.sort()
        for file in files:
            dict_tabs.append(to_point(bwc.read_bwc(file)))
        obs.append(xr.concat(dict_tabs,'point'))
    bothmasts=xr.concat(obs,'point')
    bothmasts.attrs.update(dict_tabs[0].attrs)
    bothmasts_utm = reproject(bothmasts, 32632)
    bothmasts_utm.attrs['header'] = 'Binned wind climates (bwc) from Østerild from the Northerly and Southerly masts'

    return bothmasts_utm

def set_config():
    conf = pw.wasp.Config("WAsP_12.7")
    return conf


def plot_transect_ws(tabs,flow):
    tabs = pw.wasp.add_met_fields(tabs, air_dens=1.225)
    flow = pw.wasp.add_met_fields(flow, air_dens=1.225)
    plt.plot(flow.coords['south_north'].values/1000, flow.wspd.values.transpose(),'--', label='WAsP')
    plt.plot(tabs.coords['south_north'].values[3]/1000, tabs.wspd.values[3],'o', ms=8,label='South mast')
    plt.plot(tabs.coords['south_north'].values[3 + 8]/1000, tabs.wspd.values[3 + 8],'ko', ms=8, label='North mast')
    plt.legend(loc=3, borderaxespad=1)
    plt.xlabel('Northing (km)')
    plt.ylabel('Wind speed [m/s]')
    plt.show()


def plot_transect_pd(tabs,flow):
    tabs = pw.wasp.add_met_fields(tabs, air_dens=1.225)
    flow = pw.wasp.add_met_fields(flow, air_dens=1.225)
    plt.plot(flow.coords['south_north'].values/1000, flow.power_density.values.transpose(),'--', label='WAsP')
    plt.plot(tabs.coords['south_north'].values[3]/1000, tabs.power_density.values[3],'o', ms=8,label='South mast')
    plt.plot(tabs.coords['south_north'].values[3 + 8]/1000, tabs.power_density.values[3 + 8],'ko', ms=8, label='North mast')
    plt.legend(loc=3, borderaxespad=1)
    plt.xlabel('Northing (km)')
    plt.ylabel('Power density [W/m^2]')
    plt.show()


def plot_profile(tabs,flow):
    tabs = pw.wasp.add_met_fields(tabs, air_dens=1.225)
    flow = pw.wasp.add_met_fields(flow, air_dens=1.225)
    plt.plot(flow.power_density.values,tabs.height, '--',label="WAsP")
    plt.plot(tabs.power_density.values,tabs.height,'o',label="Measurements")
    plt.legend(loc=2, borderaxespad=1)
    plt.ylabel('Height (m)')
    plt.xlabel('Power density [W/m^2]')
    plt.show()

def plot_transect_z0(tabs,transect,tmap,conf):
    z0 = tmap.get_site_effects(transect, conf, 12)["z0meso"].mean("sector")

    # Draw plot
    plt.plot(transect.coords['south_north'].values/1000, z0)
    plt.xlabel('Northing (km)')
    plt.ylabel(r'Mesoscale roughness length [m]')
    plt.show()

def plot_transect_displ(tabs,transect,tmap,conf):
    d = tmap.get_site_effects(transect, conf, 12)["displ"].mean("sector")

    # Draw plot
    plt.plot(transect.coords['south_north'].values/1000,d)
    plt.xlabel('Northing (km)')
    plt.ylabel(r'Displacement height [m]')
    plt.show()
