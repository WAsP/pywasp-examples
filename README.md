# **pywasp examples / tutorials **

This repository contains tutorials, i.e., [pywasp](https://gitlab-internal.windenergy.dtu.dk/WAsP/WAsP/pywasp) examples provided in the form of [jupyter](https://jupyter.org/) notebooks. The repository is a submodule of [pywasp](https://gitlab-internal.windenergy.dtu.dk/WAsP/WAsP/pywasp). The purpose of the examples are to familiarize users with [pywasp](https://gitlab-internal.windenergy.dtu.dk/WAsP/WAsP/pywasp) and enable them to quickly build their own workflows with [pywasp](https://gitlab-internal.windenergy.dtu.dk/WAsP/WAsP/pywasp). These examples, split into 5 separate tutorials, cover common use cases of [pywasp](https://gitlab-internal.windenergy.dtu.dk/WAsP/WAsP/pywasp).


## [**Tutorial 1: mapping of wind resources**](./tutorial_1)
This [tutorial](./tutorial_1) introduces the basic functionalities of `pywasp`, such as:
  - Opening and inspecting wind climate files;
  - Opening and inspecting raster map files, including 
       - surface elevation data (i.e., subset of [SRTM](https://www2.jpl.nasa.gov/srtm/) dataset)
       - roughness/land-use data (i.e., subset of [CORINE](https://land.copernicus.eu/pan-european/corine-land-cover) Land Cover dataset);
  - Calculating and inspecting terrain-induced effects on the wind at various sites;
  - Creating a generalized wind climate from an observed wind climate
  - Creating a resource map by "downscaling" the generalized wind climate

The users will also be introduced to [xarray](http://xarray.pydata.org), a powerful high-level package for labelled multi-dimensional arrays.

## [**Tutorial 2: analysis of roughness change model response**](./tutorial_2)
One of the aims of this [tutorial](./tutorial_2) is to show how straightforward it is to configure and run a series of simulations using `pywasp`. For that purpose, the tutorial explores the sensitivity of WAsP roughness change induced speedups to various parameters of a single roughness change line. The users will learn how to:
 - Create a single roughness change line
 - Create a series of test cases for the analysis of roughness change model response of a single roughness change line
 - Execute the series of test cases
 - Present the results of the test cases

In this tutorial we will touch upon [pandas](https://pandas.pydata.org/), python's main package for manipulation of tabular data. [pandas](https://pandas.pydata.org/) will be used to create the test case series.

## [**Tutorial 3: comparison of predicted and measured power density**](./tutorial_3)
In this [tutorial](./tutorial_3) users will get to know how to:
  - Work with `.tab` files (observed wind climate) from two different met masts
  - Work with raster maps of roughness/land-use ([CORINE](https://land.copernicus.eu/pan-european/corine-land-cover) Land Cover dataset)
  - Change or provide more elaborate roughness descriptions, using a roughness lookup table
  - Work with elevation data from lidar scans ([Geodatastyrelsen](https://eng.gst.dk/) dataset)
  - Create a generalized wind climate from the observed wind climate
  - Downscale the generalized wind climate to predict winds and power densities at a site
  - Compare downscaled and measured power density

## [**Tutorial 4: wind farm AEP calculation**](./tutorial_4)
This [tutorial](./tutorial_4) represnts a `pywasp` adaptation of the classical `WAsP` step-by-step example from the `WAsP` help. In the tutorial users will work through a complete wind turbine siting operation, starting with some measured wind data and ending up with a prediction of the power yield from erecting a turbine at a specific site. Similar to the [first tutorial](./tutorial_1), the users will get to know how to :
 - Load and work with observed wind climate (i.e., met mast data from a Portuguese site)
 - Load vector map file which contains both the roughness and terrain data
 - Load power curve of a wind turbine
 - Calculate and inspect site effects from the terrain and roughness data
 - Create a generalized wind climate from the observed wind climate
 - Downscale the generalized wind climate to the wind turbine locations
 - Calculate AEP of a single and multiple wind turbines using `pywasp` and `PyWake`


## [**Tutorial 5: importing objects from wwh files**](./tutorial_5)
In [the fifth tutorial](./tutorial_5) users will get to know how to:
 - Import different objects from `.wwh` (WAsP WorkSpace Hierarchy file)
 - Use the imported objects to generate predicted wind climate over an uniform grid
 - Export various `pywasp` `xarray` datasets as `NetCDF` files

